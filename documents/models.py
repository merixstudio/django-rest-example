from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils.models import TimeStampedModel


class Document(TimeStampedModel):

    title = models.CharField(
        _('title'),
        max_length=255,
    )
    body = models.TextField(
        _('body'),
    )
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('author'),
        related_name='documents',
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = _('document')
        verbose_name_plural = _('documents')

    def __str__(self):
        return '{} - Document #{}'.format(self.title, self.pk)
