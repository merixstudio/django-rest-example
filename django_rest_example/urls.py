from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin


urlpatterns = [
    url(
        r'^admin/',
        admin.site.urls,
    ),
    url(
        r'^api/v1/',
        include('django_rest_example.urls_v1'),
    ),
]

if settings.DEBUG:
    urlpatterns += [
        url(r'^api-auth/', include(
            'rest_framework.urls',
            namespace='rest_framework',
        )),
    ]
