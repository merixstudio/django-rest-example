from django.conf.urls import url, include

from rest_framework import routers

import auth_ex.views
import documents.views


router = routers.DefaultRouter()
router.register(r'users', auth_ex.views.UserViewSet)
router.register(r'documents', documents.views.DocumentViewSet)

urlpatterns = [
    url(
        r'^',
        include(router.urls)
    ),
]
